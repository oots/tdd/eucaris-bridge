export interface DomibusMessageHeader {
  Messaging: {
    UserMessage: {
      MessageInfo?: {
        Timestamp?: string;
        MessageId?: string;
        RefToMessageId?: string;
      };
      PartyInfo: {
        From: {
          PartyId: {
            attributes: {
              type: string;
            };
            $value: string;
          };
          Role: string;
        };
        To: {
          PartyId: {
            attributes: {
              type: string;
            };
            $value: string;
          };
          Role: string;
        };
      };
      CollaborationInfo: {
        Service: {
          attributes: {
            type: string;
          };
          $value: string;
        };
        Action: string;
        ConversationId?: string;
      };
      MessageProperties: {
        Property: {
          attributes: {
            name: string;
          };
          $value: string;
        }[];
      };
      PayloadInfo: {
        PartInfo: {
          attributes: {
            href: string;
          };
          PartProperties: {
            Property: {
              attributes: {
                name: string;
              };
              $value: string;
            }[];
          };
        }[];
      };
    };
  };
}

export interface DomibusMessageBody {
  payload: {
    attributes: {
      payloadId: string;
      contentType: string;
    };
    value: string;
  }[];
}
