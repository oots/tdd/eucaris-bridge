/**
 * Based on:
 * https://ec.europa.eu/digital-building-blocks/wikis/display/TDD/4.5.1+-+Evidence+Request+Syntax+Mapping+-+Q4+2022#id-4.5.1EvidenceRequestSyntaxMappingQ42022-1.ExchangeDataModel:QueryRequest(EvidenceRequest)
 */
export interface OOTSQueryRequest {
  _declaration: {
    _attributes: {
      version: string;
      encoding: string;
    };
  };
  'query:QueryRequest': {
    _attributes: {
      id: string;
      lang?: string;
    };
    'rim:Slot': {
      _attributes: {
        name: string;
      };
      'rim:SlotValue': {
        _attributes: {
          'xsi:type': string;
          collectionType?: string;
        };
        'rim:Value'?: {
          _text: string;
        };
        'rim:Element'?: any;
        'sdg:Agent'?: any;
      };
    }[];
    'query:ResponseOption': {
      _attributes: {
        returnType: string;
      };
    };
    'query:Query': {
      _attributes: {
        queryDefinition: string;
      };
      'rim:Slot': (NaturalPersonSlot | LegalPersonSlot)[];
    };
  };
}

export interface OOTSQueryResponse {
  _declaration: {
    _attributes: {
      version: string;
      encoding: string;
    };
  };
  'query:QueryResponse': {
    _attributes: {
      requestId: string;
      status?: string;
    };
    'rim:Slot': {
      _attributes: {
        name: string;
      };
      'rim:SlotValue': {
        _attributes: {
          'xsi:type': string;
          collectionType?: string;
        };
        'rim:Value'?: {
          _text: string;
        };
        'rim:Element'?: any;
        'sdg:Agent'?: any;
      };
    }[];
    'rim:VatResponse'?: any;
    'rim:AviResponse'?: any;
    'rim:RegistryObjectList': {
      'rim:RegistryObject': RegistryObject;
    }[];
  };
}

export interface RegistryObject {
  'rim:Slot': {
    _attributes: {
      name: string;
    };
    'rim:SlotValue': {
      _attributes: {
        'xsi:type': string;
        id: string;
      };
      'rim:Slot': {
        _attributes: {
          name: 'EvidenceMetadata';
        };
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': string;
          };
          'sdg:Evidence': {
            Identifier: {
              _text: string;
            };
            IsAbout: {
              NaturalPerson?: {
                Identifier: {
                  _attributes: {
                    schemeID: string;
                  };
                  _text: string;
                };
                FamilyName: {
                  _text: string;
                };
                GivenName: {
                  _text: string;
                };
                DateOfBirth: {
                  _text: string;
                };
              };
              LegalPerson?: {
                LegalPersonIdentifier: {
                  _attributes: {
                    schemeID: string;
                  };
                  _text: string;
                };
                LegalName: {
                  _text: string;
                };
              };
            };
            IssuingAuthority: {
              Identifier: {
                _attributes: {
                  schemeID: string;
                };
                _text: string;
              };
              Name: {
                _attributes: {
                  lang: string;
                };
                _text: string;
              };
            };
            IsConformantTo: {
              EvidenceTypeClassification: {
                _text: string;
              };
              Title: {
                _text: string;
              };
              Description: {
                _text: string;
              };
            };
            IssuingDate: {
              _text: string;
            };
            Distribution: {
              Format: {
                _text: string;
              };
            };
            ValidityPeriod: {
              StartDate: {
                _text: string;
              };
              EndDate: {
                _text: string;
              };
            };
          };
        };
      };
      'rim:RepositoryItemRef': {
        _attributes: {
          'xlink:href': string;
          'xlink:title': string;
        };
      };
    };
  };
}

/**
 * Natural person slot
 */
export interface NaturalPersonSlot {
  _attributes: {
    name: 'NaturalPerson';
  };
  'rim:SlotValue': {
    _attributes: {
      'xsi:type': 'rim:AnyValueType';
    };
    'sdg:Person': {
      'sdg:LevelOfAssurance': {
        _text: string;
      };
      'sdg:Identifier'?: {
        attributes: {
          schemeID: string;
        };
        $value: string;
      };
      'sdg:FamilyName': {
        _text: string;
      };
      'sdg:GivenName': {
        _text: string;
      };
      'sdg:DateOfBirth': {
        _text: string;
      };
      'sdg:BirthName'?: {
        _text: string;
      };
      'sdg:PlaceOfBirth'?: {
        _text: string;
      };
      'sdg:CurrentAddress'?: Address;
      'sdg:SectorSpecificAttribute'?: SectorSpecificAttribute[];
    };
  };
}

/**
 * Legal person slot
 */
export interface LegalPersonSlot {
  _attributes: {
    name: 'LegalPerson';
  };
  'rim:SlotValue': {
    _attributes: {
      'xsi:type': 'rim:AnyValueType';
    };
    'sdg:LegalPerson': {
      'sdg:LevelOfAssurance': {
        _text: string;
      };
      'sdg:LegalPersonIdentifier'?: {
        attributes: {
          schemeID: string;
        };
        $value: string;
      };
      'sdg:Identifier'?: {
        attributes: {
          schemeID: string;
        };
        $value: string;
      }[];
      'sdg:LegalName': {
        _text: string;
      };
      'sdg:RegisteredAddress'?: Address; //TODO: adjust these types so they can be used by the XML library
      'sdg:SectorSpecificAttribute'?: SectorSpecificAttribute[];
    };
  };
}

interface SectorSpecificAttribute {
  'sdg:AttributeName': string;
  'sdg:AttributeURI': string;
  'sdg:AttributeValue': string;
}

interface Address {
  'sdg:FullAddress'?: string;
  'sdg:Thoroughfare'?: string;
  'sdg:LocatorDesignator'?: string;
  'sdg:AdminUnitLevel1'?: string;
  'sdg:AdminUnitLevel2'?: string;
  'sdg:PostCode'?: string;
  'sdg:PostCityName'?: string;
}
