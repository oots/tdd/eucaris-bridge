import { DomibusMessageHeader } from '../internal/domibus';

interface PartyInfo {
  id: string;
  role: string;
  type: string;
}

interface CollaborationInfo {
  service: string;
  action: string;
  type: string;
  conversationId?: string;
}

interface MessageInfo {
  originalSender: string;
  finalRecipient: string;
}

interface PartInfo {
  href: string;
  contentType: string;
  value: string;
}

export interface PayloadInfo {
  parts: PartInfo[];
}

export interface requestPayload {
  data: {
    name: string;
    surName: string;
    dateOfBirth: string;
    typeOfPerson: number; // 0 -> NP, 1 -> LP
  };
}

export interface SubmitMessageHeaderParams {
  fromParty: PartyInfo;
  toParty: PartyInfo;
  collaborationInfo: CollaborationInfo;
  messageInfo: MessageInfo;
  refToMessageId?: string;
}

export interface SubmitMessageBodyParams {
  payload: PayloadInfo;
}

export interface SubmitMessageResponse {
  messageId: string;
}

export interface ListPendingMessagesResponse {
  messageIds: string[];
}

export interface RetrieveMessageResponse {
  header: DomibusMessageHeader;
  body: any;
}
