import { createClientAsync } from 'soap';
import { Result } from '../../src/utils/result';
import {
  ListPendingMessagesResponse,
  RetrieveMessageResponse,
  SubmitMessageBodyParams,
  SubmitMessageHeaderParams,
  SubmitMessageResponse,
} from './types/external';

/**
 * Function to list pending messages at a given Domibus endpoint
 *
 * @param url - The URL of the Domibus endpoint
 */
export const listPendingMessages = async (
  url: string,
): Promise<Result<ListPendingMessagesResponse>> => {
  const client = await createClientAsync(url);

  try {
    const res = await client.listPendingMessagesAsync({});
    return {
      success: true,
      data: { messageIds: res[0] ? res[0].messageID : [] },
    };
  } catch (err) {
    return {
      success: false,
      error: err,
    };
  }
};

/**
 * Function to submit a message to a given Domibus endpoint
 *
 * @param url - The URL of the Domibus endpoint
 * @param headerParams - The header parameters for the message
 * @param bodyParams - The body parameters for the message
 */
export const submitMessage = async (
  url: string,
  headerParams: SubmitMessageHeaderParams,
  bodyParams: SubmitMessageBodyParams,
): Promise<Result<SubmitMessageResponse>> => {
  const client = await createClientAsync(url);

  if (
    !client.wsdl.xmlnsInEnvelope.includes(
      ' xmlns:rs="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0"',
    )
  ) {
    client.wsdl.xmlnsInEnvelope +=
      // ' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"' +
      ' xmlns:rs="urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0"' +
      ' xmlns:sdg="http://data.europa.eu/p4s"' +
      ' xmlns:rim="urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0"' +
      ' xmlns:query="urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0"';
  }

  const { fromParty, toParty, collaborationInfo, messageInfo, refToMessageId } =
    headerParams;
  const { payload } = bodyParams;

  client.addSoapHeader(
    {
      Messaging: {
        UserMessage: {
          ...(refToMessageId && {
            MessageInfo: { RefToMessageId: refToMessageId },
          }),
          PartyInfo: {
            From: {
              PartyId: {
                attributes: {
                  type: fromParty.type,
                },
                $value: fromParty.id,
              },
              Role: fromParty.role,
            },
            To: {
              PartyId: {
                attributes: {
                  type: toParty.type,
                },
                $value: toParty.id,
              },
              Role: toParty.role,
            },
          },
          CollaborationInfo: {
            Service: {
              attributes: {
                type: collaborationInfo.type,
              },
              $value: collaborationInfo.service,
            },
            Action: collaborationInfo.action,
            // Add ConversationId if it exists
            ...(collaborationInfo.conversationId && {
              ConversationId: collaborationInfo.conversationId,
            }),
          },
          MessageProperties: {
            Property: [
              {
                attributes: {
                  name: 'originalSender',
                },
                $value: messageInfo.originalSender,
              },
              {
                attributes: { name: 'finalRecipient' },
                $value: messageInfo.finalRecipient,
              },
            ],
          },
          PayloadInfo: {
            PartInfo: payload.parts.map((part) => ({
              attributes: {
                href: `cid:${part.href}`,
              },
              PartProperties: {
                Property: [
                  {
                    attributes: {
                      name: 'MimeType',
                    },
                    $value: part.contentType,
                  },
                ],
              },
            })),
          },
        },
      },
    },
    '',
    'ns1',
  );

  try {
    const res = await client.submitMessageAsync({
      payload: payload.parts.map((part) => ({
        attributes: {
          payloadId: `cid:${part.href}`,
          contentType: part.contentType,
        },
        value: part.value,
      })),
    });

    // console.log(client.lastRequest);

    return {
      success: true,
      data: { messageId: res[0].messageID[0] },
    };
  } catch (err) {
    return {
      success: false,
      error: err,
    };
  }
};

/**
 * Function to retrieve a message from a given Domibus endpoint
 *
 * @param url - The URL of the Domibus endpoint
 * @param messageId - The ID of the message to retrieve
 */
export const retrieveMessage = async (
  url: string,
  messageId: string,
): Promise<Result<RetrieveMessageResponse>> => {
  const client = await createClientAsync(url);

  try {
    const res = await client.retrieveMessageAsync({
      messageID: messageId,
    });

    return {
      success: true,
      data: {
        header: res[2],
        body: res[0],
      },
    };
  } catch (err) {
    return {
      success: false,
      error: err,
    };
  }
};

// const test123 = async () => {
//   const res = await retrieveMessage(
//     'http://oots-rel-1.westeurope.cloudapp.azure.com:8782/domibus/services/backend?wsdl',
//     '8810dbea-bcd7-11ed-ae62-0242ac120002@domibus.eu',
//   );

//   if (res.success) {
//     console.log(JSON.stringify(res.data, null, 2));
//     return;
//   }

//   console.log(res.error);
// };

// test123().catch(console.error);
