import { isError } from '../../../src/utils/result';
import { listPendingMessages, retrieveMessage, submitMessage } from '../ws';
import {
  SUBMIT_MESSAGE_EXAMPLE_1,
  SUBMIT_MESSAGE_EXAMPLE_2,
} from './constants';
import { sleep } from './utils';

import * as dotenv from 'dotenv';
dotenv.config();

const C2 = `${process.env.CLIENT_ACCESS_POINT}?wsdl`;

const C3 = `${process.env.SERVER_ACCESS_POINT}?wsdl`;

describe('Domibus', () => {
  beforeEach(async () => {
    await sleep(1000);

    // Retrieve all pending messages from C2 and C3
    const pendingMessagesC2 = await listPendingMessages(C2);
    const pendingMessagesC3 = await listPendingMessages(C3);

    // Check if the response is an error
    const isErrC2 = isError(pendingMessagesC2);
    const isErrC3 = isError(pendingMessagesC3);

    if (isErrC2 || isErrC3)
      throw Error('[beforeEach]: Error listing pending messages');

    // Retrieve all pending messages from C2 and C3
    const pendingMessagesC2Ids = pendingMessagesC2.data.messageIds;
    const pendingMessagesC3Ids = pendingMessagesC3.data.messageIds;

    // Check if both empty
    if (pendingMessagesC2Ids.length === 0 && pendingMessagesC3Ids.length === 0)
      return;

    // Delete all pending messages from C2 and C3
    for (const messageId of pendingMessagesC2Ids) {
      await retrieveMessage(C2, messageId);
    }

    for (const messageId of pendingMessagesC3Ids) {
      await retrieveMessage(C3, messageId);
    }

    await sleep(1000);
  });

  describe('submitMessage', () => {
    it('Should suceed submitting a message to C2', async () => {
      // Submit message to C2
      const res = await submitMessage(
        C2,
        SUBMIT_MESSAGE_EXAMPLE_1.headerParams,
        SUBMIT_MESSAGE_EXAMPLE_1.bodyParams,
      );

      // Check if the response is an error
      const isErr = isError(res);
      expect(isErr).toBe(false);
      if (isErr) throw Error('Error submitting message to C2');

      // Check if the response contains a message id
      expect(res.data.messageId).toBeDefined();

      expect.assertions(2);
    });

    it('Should suceed submitting a message to C3', async () => {
      const res = await submitMessage(
        C3,
        SUBMIT_MESSAGE_EXAMPLE_2.headerParams,
        SUBMIT_MESSAGE_EXAMPLE_2.bodyParams,
      );

      const isErr = isError(res);
      expect(isErr).toBe(false);

      if (isErr) throw Error('Error submitting message to C3');
      expect(res.data.messageId).toBeDefined();

      expect.assertions(2);
    });
  });

  describe('listPendingMessages', () => {
    it('Should list pending messages at C2 - empty', async () => {
      const res = await listPendingMessages(C2);

      const isErr = isError(res);
      expect(isErr).toBe(false);

      if (isErr) throw Error('Error listing pending messages at C2');
      expect(res.data).toEqual({ messageIds: [] });

      expect.assertions(2);
    });

    it('Should list pending messages at C3 - empty', async () => {
      const res = await listPendingMessages(C3);

      const isErr = isError(res);
      expect(isErr).toBe(false);

      if (isErr) throw Error('Error listing pending messages at C3');
      expect(res.data).toEqual({ messageIds: [] });

      expect.assertions(2);
    });

    it('Should list pending messages at C2 - not empty', async () => {
      // Submit message to C3
      const res = await submitMessage(
        C3,
        SUBMIT_MESSAGE_EXAMPLE_2.headerParams,
        SUBMIT_MESSAGE_EXAMPLE_2.bodyParams,
      );

      // Check if the response is an error
      const isErr = isError(res);
      expect(isErr).toBe(false);
      if (isErr) throw Error('Error submitting message to C3');

      // Check if the response contains a message id
      expect(res.data.messageId).toBeDefined();

      // Wait for the message to be processed and sent to C2
      await sleep(1000);

      // List pending messages at C2
      const res2 = await listPendingMessages(C2);

      // Check if the response is an error
      const isErr2 = isError(res2);
      expect(isErr2).toBe(false);
      if (isErr2) throw Error('Error listing pending messages at C2');

      expect(res2.data.messageIds.length).toBe(1);
      expect(res2.data.messageIds[0]).toBe(res.data.messageId);
    });

    it('Should list pending messages at C3 - not empty', async () => {
      // Submit message to C2
      const res = await submitMessage(
        C2,
        SUBMIT_MESSAGE_EXAMPLE_1.headerParams,
        SUBMIT_MESSAGE_EXAMPLE_1.bodyParams,
      );

      // Check if the response is an error
      const isErr = isError(res);
      expect(isErr).toBe(false);
      if (isErr) throw Error('Error submitting message to C2');

      // Check if the response contains a message id
      expect(res.data.messageId).toBeDefined();

      // Wait for the message to be processed and sent to C3
      await sleep(1000);

      // List pending messages at C3
      const res2 = await listPendingMessages(C3);

      // Check if the response is an error
      const isErr2 = isError(res2);
      expect(isErr2).toBe(false);
      if (isErr2) throw Error('Error listing pending messages at C3');

      expect(res2.data.messageIds.length).toBe(1);
      expect(res2.data.messageIds[0]).toBe(res.data.messageId);
    });
  });

  describe('retrieveMessage', () => {
    it('Should succeed retrieving a message from C2', async () => {
      // Submit message to C3
      const res = await submitMessage(
        C3,
        SUBMIT_MESSAGE_EXAMPLE_2.headerParams,
        SUBMIT_MESSAGE_EXAMPLE_2.bodyParams,
      );

      // Check if the response is an error
      const isErr = isError(res);
      expect(isErr).toBe(false);
      if (isErr) throw Error('Error submitting message to C2');

      // Check if the response contains a message id
      expect(res.data.messageId).toBeDefined();

      // Wait for the message to be processed and sent to C3
      await sleep(1000);

      // Retrieve message from C3
      const res2 = await retrieveMessage(C2, res.data.messageId);
      // Check if the response is an error
      const isErr2 = isError(res2);
      expect(isErr2).toBe(false);
      if (isErr2) throw Error('Error retrieving message from C2');

      // Check if the response contains a body and a header
      expect(res2.data.body).toBeDefined();
      expect(res2.data.header).toBeDefined();

      expect.assertions(5);
    });
    it('Should succeed retrieving a message from C3', async () => {
      // Submit message to C2
      const res = await submitMessage(
        C2,
        SUBMIT_MESSAGE_EXAMPLE_1.headerParams,
        SUBMIT_MESSAGE_EXAMPLE_1.bodyParams,
      );

      // Check if the response is an error
      const isErr = isError(res);
      expect(isErr).toBe(false);
      if (isErr) throw Error('Error submitting message to C2');

      // Check if the response contains a message id
      expect(res.data.messageId).toBeDefined();

      // Wait for the message to be processed and sent to C3
      await sleep(1000);

      // Retrieve message from C3
      const res2 = await retrieveMessage(C3, res.data.messageId);

      // Check if the response is an error
      const isErr2 = isError(res2);
      expect(isErr2).toBe(false);
      if (isErr2) throw Error('Error retrieving message from C3');

      // Check if the response contains a body and a header
      expect(res2.data.body).toBeDefined();
      expect(res2.data.header).toBeDefined();

      expect.assertions(5);
    });
  });

  describe('Complete flow', () => {
    it('C1 to C4 && C4 to C1', async () => {
      // Submit message to C2
      const res = await submitMessage(
        C2,
        SUBMIT_MESSAGE_EXAMPLE_1.headerParams,
        SUBMIT_MESSAGE_EXAMPLE_1.bodyParams,
      );

      // Check if the response is an error
      const isErr = isError(res);
      expect(isErr).toBe(false);
      if (isErr) throw Error('Error submitting message to C2');

      // Check if the response contains a message id
      expect(res.data.messageId).toBeDefined();

      // Wait for the message to be processed and sent to C3
      await sleep(1500);

      // Retrieve message from C3
      const res2 = await retrieveMessage(C3, res.data.messageId);

      // Check if the response is an error
      const isErr2 = isError(res2);
      expect(isErr2).toBe(false);
      if (isErr2) throw Error('Error retrieving message from C3');

      // Submit message to C3 (same message with added RefToMessageId)
      const res3 = await submitMessage(
        C3,
        {
          ...SUBMIT_MESSAGE_EXAMPLE_2.headerParams,
          refToMessageId: res.data.messageId, // We added refToMessageId
        },
        SUBMIT_MESSAGE_EXAMPLE_2.bodyParams,
      );

      // Check if the response is an error
      const isErr3 = isError(res3);
      expect(isErr3).toBe(false);
      if (isErr3) throw Error('Error submitting message to C3');

      // Wait for the message to be processed and sent to C2
      await sleep(1500);

      // Retrieve message from C2
      const res4 = await retrieveMessage(C2, res3.data.messageId);

      // Check if the response is an error
      const isErr4 = isError(res4);
      expect(isErr4).toBe(false);
      if (isErr4) throw Error('Error retrieving message from C2');

      // Check if the response contains a correct RefToMessageId
      expect(
        res4.data.header.Messaging.UserMessage.MessageInfo?.RefToMessageId,
      ).toBe(res.data.messageId);

      // console.log(JSON.stringify(res4.data));

      expect.assertions(6);
    });
  });
});
