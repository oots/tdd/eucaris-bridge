import { VatAllVhohRequest } from '../../types/internal/vat';
import { EucarisHeader } from '../../types/internal/eucaris';

export const VAT_ALL_VHOH_REQUEST_EXAMPLE = {
  params: {
    infoReq: {
      VATAllVHOHRequest: {
        attributes: { xmlns: '' },
        Header: {
          // THIS NEEDS TO ALWAYS BE UNIQUE
          MessageID: '36f8095d-4ef1-4af6-ab6c-0a801779723c',
          MessageVersion: '1.0',
          ServiceExecutionReason: {
            ServiceExecutionReasonCode: '0',
            ServiceExecutionReasonDesc: 'Not specified',
          },
          RecipientCountry: 'NL',
          SenderCountry: 'NL',
          SenderOrganisation: {
            SenderOrganisationCode: '0',
            SenderOrganisationDesc: 'NOT SPECIFIED',
          },
          SenderName: 'SDGBridge',
          TimeStamp: '2023-02-01T13:13:32.6772597Z',
          TimeOut: 0,
        } as EucarisHeader,
        Body: {
          InformationRequest: {
            CaseInformation: {
              RequestId: '100eab68-e03c-4797-8150-0fe8a9b574e9',
              CaseHandlingOrganisationName: 'SDG',
              ReferenceDateTime: '2022-01-01T12:00:00+00:00',
            },
            PersonOfInterestSearchData: {
              NumberData: {
                IdentificationNumbers: {
                  IdentificationNumber: [
                    {
                      IdTypeCode: 'PIN',
                      IdNumber: '984444743102255',
                    },
                  ],
                },
              },
            },
          },
        } as VatAllVhohRequest,
      },
    },
  },
};
