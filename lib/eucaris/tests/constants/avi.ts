import { AviReqByChassis, AviReqByRegNum } from '../../types/internal/avi';
import { EucarisHeader } from '../../types/internal/eucaris';

export const AVI_REQ_BY_CHASSIS_EXAMPLE = {
  bodyParams: {
    infoReq: {
      AVIReqByChassis: {
        attributes: { xmlns: '' },
        Header: {
          MessageID: '268fdaf7-1890-40fe-b5a2-35adc6ea7fce',
          MessageVersion: '1.0',
          ServiceExecutionReason: {
            ServiceExecutionReasonCode: '5',
            ServiceExecutionReasonDesc: 'Test',
          },
          RecipientCountry: 'NL',
          SenderCountry: 'NL',
          SenderOrganisation: {
            SenderOrganisationCode: '0',
            SenderOrganisationDesc: 'Not specified',
          },
          SenderName: 'SDGBridge',
          TimeStamp: '2023-02-01T13:14:05.7491643Z',
          TimeOut: 0,
        } as EucarisHeader,
        Body: {
          VehicleIdentificationNumber: 'BK567ABK567ABK567',
          MileageInfoRequested: false,
        } as AviReqByChassis,
      },
    },
  },
};

export const AVI_REQ_BY_REG_NUM_EXAMPLE = {
  bodyParams: {
    infoReq: {
      AVIReqByRegNum: {
        attributes: { xmlns: '' },
        Header: {
          MessageID: 'db727056-985b-44fa-9234-bc7a26ae3001',
          MessageVersion: '1.0',
          ServiceExecutionReason: {
            ServiceExecutionReasonCode: '0',
            ServiceExecutionReasonDesc: 'Not specified',
          },
          RecipientCountry: 'D',
          SenderCountry: 'NL',
          SenderOrganisation: {
            SenderOrganisationCode: '0',
            SenderOrganisationDesc: 'Not specified',
          },
          SenderName: 'Test001',
          TimeStamp: '2018-10-29T10:25:42Z',
          TimeOut: 0,
        } as EucarisHeader,
        Body: {
          VehRegistrationNumber: {
            VehRegistrationNumberPart1: 'VS',
            VehRegistrationNumberPart2: 'ML 77001',
          },
          MileageInfoRequested: true,
        } as AviReqByRegNum,
      },
    },
  },
};
