export type AviReqByChassisParams = {
  vehicleIdentificationNumber: string;
  mileageInfoRequested?: boolean;
};

export type VatAllVhohRequestParams = {
  messageID: string;
  caseInformation: {
    requestId: string;
    caseHandlingOrganisationName: string;
  };
} & (
  | {
      legalEntityCode: 'NP';
      query: NPQuery;
    }
  | {
      legalEntityCode: 'O';
      query: LPQuery;
    }
);

export type NPQuery = {
  surname: string;
  dateOfBirth: string;
  forenames?: string;
};

export type LPQuery = {
  name: string;
};
