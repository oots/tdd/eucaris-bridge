/**
 * Interface for the AVIReqByRegNum body
 *
 * Described in ???
 */
export interface AviReqByRegNum {
  VehRegistrationNumber: {
    VehRegistrationNumberPart1: string;
    VehRegistrationNumberPart2?: string;
  };
  MileageInfoRequested: boolean;
}

/**
 * Interface for the AVIReqByChassis body
 *
 * Described in ???
 */
export interface AviReqByChassis {
  VehicleIdentificationNumber: string;
  MileageInfoRequested: boolean;
}
