/**
 * Interface for the VATAllVhohRequest body
 *
 * Described in section 4.5.3
 */
export interface VatAllVhohRequest {
  InformationRequest: {
    CaseInformation: {
      RequestId: string;
      CaseHandlingOrganisationName: string;
      ReferenceDateTime?: string;
    };
    PersonOfInterestSearchData: {
      NameAndEventuallyNumberData?: {
        LegalEntityCode: string;
        NaturalPersonSearchData?: {
          Surname: string;
          Forenames?: string;
          DateOfBirth: string;
        };
        LegalPersonSearchData?: {
          LegalPersonName: string;
        };
        IdentificationNumbers?: {
          IdentificationNumber: {
            IdTypeCode: string;
            IdNumber: string;
          }[];
        };
      };
      NumberData?: {
        IdentificationNumbers: {
          IdentificationNumber: {
            IdTypeCode: string;
            IdNumber: string;
          }[];
        };
      };
    };
    PersonOfInterestRefinementData?: {
      NaturalPersonRefinementData?: {
        VehHolderOtherNames?: {
          VehHolderMiddleName?: string;
          VehHolderOtherName?: string;
        };
        VehHolderPlaceOfBirth?: string;
        VehHolderGenderCode?: string;
      };
      AddressRefinementData?: {
        VehHolderAddrCareOfName?: string;
        VehHolderAddrStreetName?: string;
        VehHolderAddrStreetNameExtra?: string;
        VehHolderAddrStreetNumber?: string;
        VehHolderAddrStreetNrAnnex?: string;
        VehHolderAddrPostcode?: string;
        VehHolderAddrPlaceOfResidence?: string;
        VehHolderAddrCountryCode?: string;
      };
      VehicleRefinementData?: {
        VehIdentificationNumber?: string;
        VehRegistrationNumber?: {
          VehRegistrationNumberPart1: string;
          VehRegistrationNumberPart2?: string;
        };
      };
    };
  };
}
