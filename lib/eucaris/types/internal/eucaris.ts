/**
 * Interface for the Header present in all Eucaris requests
 *
 * Described in section 4.2
 */
export interface EucarisHeader {
  MessageID: string;
  MessageRefID?: string;
  MessageVersion: string;
  ServiceExecutionReason: {
    ServiceExecutionReasonCode: string;
    ServiceExecutionReasonDesc: string;
  };
  ServiceFileNumber?: string;
  RecipientCountry: string;
  RecipientCountryTable?: {
    RecipientCountryListValue: string[];
  };
  SenderCountry: string;
  SenderOrganisation: {
    SenderOrganisationCode: string;
    SenderOrganisationDesc: string;
  };
  SenderName: string;
  SenderOrganisationName?: string;
  TimeStamp: string;
  TimeOut: number;
}
