import { createClientAsync } from 'soap';
import { Result } from '../../src/utils/result';
import axios, { AxiosInstance } from 'axios';
import * as https from 'https';
import { AVI_REQ_BY_REG_NUM_EXAMPLE } from './tests/constants';
import { AviReqByChassis, EucarisHeader } from './types/internal';
import {
  AviReqByChassisParams,
  VatAllVhohRequestParams,
} from './types/external';
import { prepareVatAllVhohRequest } from './utils';
import { randomUUID } from 'crypto';

const EUCARIS_WSDL =
  'https://eucaris-sdg.westeurope.cloudapp.azure.com/EUCARIS.Public/Genericservice.asmx?WSDL';

export const aviRequestByRegNum = async (
  url: string,
): Promise<Result<{ header: any; body: any }>> => {
  const httpsClient: AxiosInstance = axios.create({
    httpsAgent: new https.Agent({
      rejectUnauthorized: false,
    }),
  });

  const client = await createClientAsync(url, { request: httpsClient });

  const res = await client.GetInfoAsync(AVI_REQ_BY_REG_NUM_EXAMPLE.bodyParams);

  const aviResposne = res[0].GetInfoResult.AVIResponse;

  return {
    success: true,
    data: { header: aviResposne.Header, body: aviResposne.Body },
  };
};

export const aviRequestByChassis = async (
  url: string,
  params: AviReqByChassisParams,
): Promise<Result<{ header: any; body: any }>> => {
  const httpsClient: AxiosInstance = axios.create({
    httpsAgent: new https.Agent({
      rejectUnauthorized: false,
    }),
  });

  const client = await createClientAsync(url, { request: httpsClient });

  const res = await client.GetInfoAsync({
    infoReq: {
      AVIReqByChassis: {
        attributes: { xmlns: '' },
        Header: {
          MessageID: '268fdaf7-1890-40fe-b5a2-35adc6ea7fce',
          MessageVersion: '1.0',
          ServiceExecutionReason: {
            ServiceExecutionReasonCode: '5',
            ServiceExecutionReasonDesc: 'Test',
          },
          RecipientCountry: 'NL',
          SenderCountry: 'NL',
          SenderOrganisation: {
            SenderOrganisationCode: '0',
            SenderOrganisationDesc: 'Not specified',
          },
          SenderName: 'SDGBridge',
          TimeStamp: '2023-02-01T13:14:05.7491643Z',
          TimeOut: 0,
        } as EucarisHeader,
        Body: {
          VehicleIdentificationNumber: params.vehicleIdentificationNumber,
          MileageInfoRequested: params.mileageInfoRequested,
        } as AviReqByChassis,
      },
    },
  });

  const aviResposne = res[0].GetInfoResult.AVIResponse;

  return {
    success: true,
    data: { header: aviResposne.Header, body: aviResposne.Body },
  };
};

export const vatAllVhohRequest = async (
  url: string,
  params: VatAllVhohRequestParams,
): Promise<Result<{ header: any; body: any }>> => {
  const httpsClient: AxiosInstance = axios.create({
    httpsAgent: new https.Agent({
      rejectUnauthorized: false,
    }),
  });

  const client = await createClientAsync(url, { request: httpsClient });

  const { legalEntityCode, query } = params;

  if (legalEntityCode === 'NP') {
    // Return error if any of the required fields are missing
    if (!query.surname || !query.dateOfBirth) {
      return {
        success: false,
        error: new Error('Missing required fields for NP query'),
      };
    }

    const data = prepareVatAllVhohRequest(params);
    const res = await client.GetInfoAsync(data);
    const vatResponse = res[0].GetInfoResult.VATAllVHOHResponse;

    return {
      success: true,
      data: { header: vatResponse.Header, body: vatResponse.Body },
    };
  } else if (legalEntityCode === 'O') {
    // Return error if any of the required fields are missing
    if (!query.name) {
      return {
        success: false,
        error: new Error('Missing required fields for LP query'),
      };
    }

    const data = prepareVatAllVhohRequest(params);
    const res = await client.GetInfoAsync(data);
    const vatResponse = res[0].GetInfoResult.VATAllVHOHResponse;

    return {
      success: true,
      data: { header: vatResponse.Header, body: vatResponse.Body },
    };
  }

  return {
    success: false,
    error: new Error('Invalid legal entity code'),
  };
};

// aviRequestByChassis(EUCARIS_WSDL, {
//   vehicleIdentificationNumber: 'BK567ABK567ABK567',
//   mileageInfoRequested: true,
// })
//   .then((res) => {
//     if (res.success) {
//       console.log(JSON.stringify(res.data));
//     }
//   })
//   .catch((err) => console.log(err));

vatAllVhohRequest(EUCARIS_WSDL, {
  caseInformation: {
    requestId: randomUUID(),
    caseHandlingOrganisationName: 'SDG',
  },
  messageID: randomUUID(),
  legalEntityCode: 'NP',
  query: {
    forenames: 'Eric',
    surname: 'Berghalter',
    dateOfBirth: '19700202',
  },
})
  .then((res) => {
    if (res.success) {
      console.log(JSON.stringify(res.data));
    }
  })
  .catch((err) => console.log(err));
