import { VatAllVhohRequestParams } from './types/external';
import { EucarisHeader, VatAllVhohRequest } from './types/internal';

export const prepareVatAllVhohRequest = (params: VatAllVhohRequestParams) => {
  const {
    messageID,
    caseInformation: { caseHandlingOrganisationName, requestId },
    legalEntityCode,
    query,
  } = params;

  const queryData =
    legalEntityCode === 'NP'
      ? {
          NaturalPersonSearchData: {
            Surname: query.surname,
            ...(query.forenames && { Forenames: query.forenames }),
            DateOfBirth: query.dateOfBirth,
          },
        }
      : {
          LegalPersonSearchData: {
            LegalPersonName: query.name,
          },
        };

  return {
    infoReq: {
      VATAllVHOHRequest: {
        attributes: { xmlns: '' },
        Header: {
          // THIS NEEDS TO ALWAYS BE UNIQUE
          MessageID: messageID,
          MessageVersion: '1.0',
          ServiceExecutionReason: {
            ServiceExecutionReasonCode: '0',
            ServiceExecutionReasonDesc: 'Not specified',
          },
          RecipientCountry: 'NL',
          SenderCountry: 'NL',
          SenderOrganisation: {
            SenderOrganisationCode: '0',
            SenderOrganisationDesc: 'NOT SPECIFIED',
          },
          SenderName: 'SDGBridge',
          TimeStamp: '2023-03-01T07:38:24.0774159Z',
          TimeOut: 0,
        } as EucarisHeader,
        Body: {
          InformationRequest: {
            CaseInformation: {
              RequestId: requestId,
              CaseHandlingOrganisationName: caseHandlingOrganisationName,
              // ReferenceDateTime: '2022-01-01T12:00:00+01:00', // TODO: Date.now()?
            },
            PersonOfInterestSearchData: {
              NameAndEventuallyNumberData: {
                LegalEntityCode: legalEntityCode,
                ...queryData,
              },
            },
          },
        } as VatAllVhohRequest,
      },
    },
  };
};
