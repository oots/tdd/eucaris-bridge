# Installing dependencies
FROM node:18.12.1-alpine as base
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile --silent --production --ignore-scripts && yarn cache clean

# Building the app
FROM base as builder
RUN yarn install --frozen-lockfile --silent && yarn cache clean
COPY nest-cli.json tsconfig*.json ./
COPY src src
COPY lib lib
RUN yarn build

# Running the app
FROM base
WORKDIR /app
ENV NODE_ENV=production
COPY --from=builder /app/dist dist
COPY .env ./
RUN chown node:node /app
USER node

EXPOSE 3001

ENV PORT 3001

CMD [ "node", "dist/src/main" ]