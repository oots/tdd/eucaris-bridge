import { Module } from '@nestjs/common';
import ServerModule from './modules/server/server.module';
import { ClientModule } from './modules/client/client.module';
import ConfigModule from './config/configuration';
import DatastoreModule from './modules/datastore/datastore.module';

@Module({
  imports: [ConfigModule, ClientModule, ServerModule, DatastoreModule],
  providers: [],
})
export class AppModule {}
export default AppModule;
