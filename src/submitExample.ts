const submitExampleMessage = async () => {
  // Send post request

  const response = await fetch('http://0.0.0.0:3000/submitMessage', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      name: 'Donna',
      surName: 'Vries',
      dateOfBirth: '19900202',
    }),
  });

  // Response
  console.log(await response.json());

  // Wait for 5 seconds
};

submitExampleMessage().catch(console.error);
