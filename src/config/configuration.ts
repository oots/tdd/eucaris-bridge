import { ConfigModule } from '@nestjs/config';
import * as Joi from 'joi';

// Split into Server and Client (so we can remove client later)
export type Config = ServerConfig & ClientConfig;

type ServerConfig = {
  SERVER_ACCESS_POINT: string;
  EUCARIS_ACCESS_POINT: string;
};

type ClientConfig = {
  CLIENT_ACCESS_POINT: string;
};

const config = (): Config => ({
  SERVER_ACCESS_POINT: process.env.SERVER_ACCESS_POINT || '',
  CLIENT_ACCESS_POINT: process.env.CLIENT_ACCESS_POINT || '',
  EUCARIS_ACCESS_POINT: process.env.EUCARIS_ACCESS_POINT || '',
});

export default ConfigModule.forRoot({
  envFilePath: '.env',
  load: [config],
  validationSchema: Joi.object({
    SERVER_ACCESS_POINT: Joi.string().required(),
    CLIENT_ACCESS_POINT: Joi.string().required(),
    EUCARIS_ACCESS_POINT: Joi.string().required(),
  }),
});
