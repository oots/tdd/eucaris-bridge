import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Config } from 'src/config/configuration';
import { ConfigService } from '@nestjs/config';
import {
  listPendingMessages,
  NaturalPersonSlot,
  RegistryObject,
  retrieveMessage,
  submitMessage,
} from '../../../lib/domibus';
import { isError } from '../../utils/result';
import { Client } from 'soap';
import { aviRequestByChassis, vatAllVhohRequest } from '../../../lib/eucaris';
import { randomUUID } from 'crypto';
import { json2xml, xml2json } from 'xml-js';
import { OOTSQueryResponseJSON } from './constants';

@Injectable()
export class ServerService {
  private wsdlUrl: string;
  private eucarisWsdlUrl: string;
  private client: Client | null = null;
  private attachment = 1;

  constructor(private configService: ConfigService<Config, true>) {
    this.wsdlUrl = `${configService.get<string>('SERVER_ACCESS_POINT')}?wsdl`;
    this.eucarisWsdlUrl = `${configService.get<string>(
      'EUCARIS_ACCESS_POINT',
    )}?wsdl`;
  }

  @Cron(CronExpression.EVERY_5_SECONDS)
  async processMessages() {
    // Load pending messages
    const pendingMessagesResult = await listPendingMessages(this.wsdlUrl);

    // Check for errors
    if (isError(pendingMessagesResult)) {
      console.error(pendingMessagesResult.error);
      return;
    }

    const { messageIds } = pendingMessagesResult.data;

    console.log('Starting to process messages...');

    await Promise.all(
      messageIds.map(async (messageId) => {
        // Handle request
        await this.handleRequest(messageId);
      }),
    );

    console.log('Finished processing messages...');
  }

  // Helper function for handling eDelivery requests
  async handleRequest(messageId: string) {
    console.log(
      `[${Date.now()}]: Starting to process message: ${messageId}...`,
    );

    // Retrieve message
    const messageResult = await retrieveMessage(this.wsdlUrl, messageId);

    console.log("Retrieved message's body...");
    // Check for errors
    if (isError(messageResult)) {
      console.error(messageResult.error);
      return;
    }

    const { header, body } = messageResult.data;

    const parts = body.payload as any[];

    const response = [];

    const OOTSQueryResponse = OOTSQueryResponseJSON;

    for (const part of parts) {
      const currentResponse = {
        payloadId: '',
        vatResponse: null,
        aviResponse: [],
        registryObjectList: [],
      } as {
        payloadId: string;
        vatResponse: any;
        aviResponse: any;
        registryObjectList: any;
      };

      currentResponse.payloadId = part.attributes.payloadId;

      // Decode base64 payload
      const stringPayload = Buffer.from(part.value, 'base64').toString('utf-8');
      const payload = JSON.parse(
        xml2json(stringPayload, {
          compact: true,
          spaces: 4,
        }),
      );

      const query = payload['query:QueryRequest']['query:Query']['rim:Slot'];

      let person = {};

      if (query._attributes.name === 'NaturalPerson') {
        const naturalPersonSlot = query as NaturalPersonSlot;

        const {
          'rim:SlotValue': {
            'sdg:Person': {
              'sdg:GivenName': { _text: givenName },
              'sdg:FamilyName': { _text: familyName },
              'sdg:DateOfBirth': { _text: dateOfBirth },
            },
          },
        } = naturalPersonSlot;

        person = {
          NaturalPerson: {
            Identifier: {
              _attributes: {
                schemeID: 'eidas',
              },
              _text: 'DK/DE/123456',
            },
            FamilyName: {
              _text: familyName,
            },
            GivenName: {
              _text: givenName,
            },
            DateOfBirth: {
              _text: dateOfBirth,
            },
          },
        };

        console.log(
          `NaturalPerson:  ${givenName} ${familyName} ${dateOfBirth.replace(
            /-/g,
            '',
          )}`,
        );
        console.log('Sending VATAllVHOH request...');

        // First query (VATAllVHOH request)
        const vatResult = await vatAllVhohRequest(this.eucarisWsdlUrl, {
          caseInformation: {
            requestId: randomUUID(),
            caseHandlingOrganisationName: 'SDG',
          },
          messageID: randomUUID(),
          legalEntityCode: 'NP',
          query: {
            surname: familyName,
            forenames: givenName,
            dateOfBirth: dateOfBirth.replace(/-/g, ''),
          },
        });

        if (isError(vatResult)) {
          console.error(vatResult.error);
          currentResponse.vatResponse = {
            error: 'There was an error while processing the VAT request.',
          };
          response.push(currentResponse);
          continue;
        }

        currentResponse.vatResponse = vatResult.data;
      } else if (query._attributes.name === 'LegalPerson') {
        const {
          'rim:SlotValue': {
            'sdg:LegalPerson': {
              'sdg:LegalName': { _text: name },
            },
          },
        } = query;

        person = {
          LegalPerson: {
            LegalPersonIdentifier: {
              _attributes: {
                schemeID: 'eidas',
              },
              _text: 'DK/DE/123456',
            },
            LegalName: {
              _text: name,
            },
          },
        };

        console.log(`LegalPerson: ${name}`);
        console.log('Sending VATAllVHOH request...');

        // First query (VATAllVHOH request)
        const vatResult = await vatAllVhohRequest(this.eucarisWsdlUrl, {
          caseInformation: {
            requestId: randomUUID(),
            caseHandlingOrganisationName: 'SDG',
          },
          messageID: randomUUID(),
          legalEntityCode: 'O',
          query: {
            name,
          },
        });

        if (isError(vatResult)) {
          console.error(vatResult.error);
          currentResponse.vatResponse = {
            error: 'There was an error while processing the VAT request.',
          };
          response.push(currentResponse);
          continue;
        }

        currentResponse.vatResponse = vatResult.data;
      } else {
        throw new Error('Invalid query type');
      }

      const infoResponse = currentResponse.vatResponse.body.InformationResponse;

      // Check if any vehicles were found
      if (
        infoResponse.InformationResponseMessages &&
        infoResponse.InformationResponseMessages.InformationResponseMessage
          .InformationResponseMessageCode == '101'
      ) {
        response.push(currentResponse);
        continue;
      }

      let listOfVehiclesHeldAndOwned =
        infoResponse.VehiclesReply.ListOfVehiclesHeldAndOwned;

      if (!Array.isArray(listOfVehiclesHeldAndOwned)) {
        listOfVehiclesHeldAndOwned = [listOfVehiclesHeldAndOwned];
      }

      // Second query (AVIReqByChassis request || AVIReqByRegNum request)
      for (const vehicle of listOfVehiclesHeldAndOwned) {
        const { VehicleIdentificationNumber } = vehicle;
        console.log(
          `VehicleIdentificationNumber: ${VehicleIdentificationNumber}`,
        );

        console.log('Sending AVIReqByChassis request...');
        // Second query (AVIReqByChassis request)
        const aviResult = await aviRequestByChassis(this.eucarisWsdlUrl, {
          vehicleIdentificationNumber: VehicleIdentificationNumber,
          mileageInfoRequested: false,
        });

        if (isError(aviResult)) {
          console.error(aviResult.error);
          currentResponse.aviResponse = {
            error: 'There was an error while processing the AVI request.',
          };
          response.push(currentResponse);
          continue;
        }

        // TODO: Hard coded for now
        const RegistryObject: RegistryObject = {
          'rim:Slot': {
            _attributes: {
              name: 'Sample Name',
            },
            'rim:SlotValue': {
              _attributes: {
                'xsi:type': 'rim:ExtrinsicObjectType',
                id: randomUUID().toString(),
              },
              'rim:Slot': {
                _attributes: {
                  name: 'EvidenceMetadata',
                },
                'rim:SlotValue': {
                  _attributes: {
                    'xsi:type': 'rim:AnyValueType',
                  },
                  'sdg:Evidence': {
                    Identifier: {
                      _text: randomUUID().toString(),
                    },
                    IsAbout: person,
                    IssuingAuthority: {
                      Identifier: {
                        _attributes: {
                          schemeID: 'urn:cef.eu:names:identifier:EAS:9930',
                        },
                        _text: 'DE73524311',
                      },
                      Name: {
                        _attributes: {
                          lang: 'en',
                        },
                        _text: 'Civil Registration Office Berlin I',
                      },
                    },
                    IsConformantTo: {
                      EvidenceTypeClassification: {
                        _text: 'CertificateOfBirth',
                      },
                      Title: {
                        _text: 'Certificate of Birth',
                      },
                      Description: {
                        _text:
                          'An official certificate of birth of a person - with first name, surname, sex, date and place of birth, which is obtained from the birth register of the place of birth.',
                      },
                    },
                    IssuingDate: {
                      _text: '1985-09-11',
                    },
                    Distribution: {
                      Format: {
                        _text: 'application/xml',
                      },
                    },
                    ValidityPeriod: {
                      StartDate: {
                        _text: '2022-05-20',
                      },
                      EndDate: {
                        _text: '2023-05-20',
                      },
                    },
                  },
                },
              },
              'rim:RepositoryItemRef': {
                _attributes: {
                  'xlink:href':
                    'cid:attachment' + this.attachment + '@example.oots.eu',
                  'xlink:title': 'Title',
                },
              },
            },
          },
        };

        this.attachment++;

        currentResponse.aviResponse.push(aviResult.data);

        const object = {
          'rim:RegistryObject': RegistryObject,
        };

        currentResponse.registryObjectList.push(object);

        response.push(currentResponse);
      }
    }

    const {
      Messaging: {
        UserMessage: {
          CollaborationInfo: {
            Action: action,
            Service: {
              attributes: { type: serviceType },
              $value: serviceValue,
            },
            ConversationId: conversationId,
          },
          PartyInfo: {
            From: {
              PartyId: {
                attributes: { type: fromType },
                $value: fromId,
              },
              Role: fromRole,
            },
            To: {
              PartyId: {
                attributes: { type: toType },
                $value: toId,
              },
              Role: toRole,
            },
          },
        },
      },
    } = header;

    OOTSQueryResponse['query:QueryResponse']['rim:RegistryObjectList'] =
      response[0].registryObjectList;

    OOTSQueryResponse['query:QueryResponse']['rim:AviResponse'] = [];

    OOTSQueryResponse['query:QueryResponse']['rim:AviResponse'] =
      response[0].aviResponse;

    OOTSQueryResponse['query:QueryResponse']['rim:VatResponse'] =
      response[0].vatResponse;

    // Send response
    const submitMessageResponse = await submitMessage(
      this.wsdlUrl,
      {
        collaborationInfo: {
          action,
          type: serviceType,
          service: serviceValue,
          ...(conversationId && { conversationId }),
        },
        fromParty: {
          id: toId,
          type: toType,
          role: toRole,
        },
        toParty: {
          id: fromId,
          type: fromType,
          role: fromRole,
        },
        // FIXME: Hardcoded for now
        messageInfo: {
          finalRecipient:
            'urn:oasis:names:tc:ebcore:partyid-type:unregistered:C1',
          originalSender:
            'urn:oasis:names:tc:ebcore:partyid-type:unregistered:C4',
        },
        refToMessageId: messageId,
      },
      {
        payload: {
          parts: [
            {
              href: 'message',
              contentType: 'text/xml',
              value: Buffer.from(
                json2xml(JSON.stringify(OOTSQueryResponse), {
                  compact: true,
                  spaces: 4,
                }),
              ).toString('base64'),
            },
          ],
        },
      },
    );

    if (isError(submitMessageResponse)) {
      console.error(submitMessageResponse.error);
      return;
    }

    console.log(
      `[${Date.now()}]: Submit message: ${
        submitMessageResponse.data.messageId
      }...`,
    );
    console.log(
      `[${Date.now()}]: Finished processing message: ${messageId}...`,
    );
  }
}

export default ServerService;
