import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import ServerService from './server.service';
import ConfigModule from '../../config/configuration';
import DatastoreModule from '../datastore/datastore.module';

@Module({
  imports: [ConfigModule, ScheduleModule.forRoot(), DatastoreModule],
  providers: [ServerService],
})
export class ServerModule {}

export default ServerModule;
