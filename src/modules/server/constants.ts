import { OOTSQueryResponse } from 'lib/domibus';

export const OOTSQueryResponseJSON: OOTSQueryResponse = {
  _declaration: {
    _attributes: {
      version: '1.0',
      encoding: 'utf-8',
    },
  },
  'query:QueryResponse': {
    _attributes: {
      requestId: '1.0', //same as ID of request
    },
    'rim:Slot': [
      {
        _attributes: {
          name: 'SpecificationIdentifier',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:StringValueType',
          },
          'rim:Value': {
            _text: 'oots-edm:v1.0',
          },
        },
      },
      {
        _attributes: {
          name: 'EvidenceResponseIdentifier',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:StringValueType',
          },
          'rim:Value': {
            _text: '5af62cce-debe-11ec-9d64-0242ac120002', //ID Of the request message
          },
        },
      },
      {
        _attributes: {
          name: 'IssueDateTime',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:DateTimeValueType',
          },
          'rim:Value': {
            _text: '2022-05-19T17:10:10.872Z',
          },
        },
      },
      {
        _attributes: {
          name: 'IssueDateTime',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:DateTimeValueType',
          },
          'rim:Value': {
            _text: '2022-05-19T17:10:10.872Z',
          },
        },
      },
      {
        _attributes: {
          name: 'EvidenceProvider',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:CollectionValueType',
          },
          'rim:Element': {
            _attributes: {
              'xsi:type': 'rim:AnyValueType',
            },
            'sdg:Agent': {
              'sdg:Identifier': {
                _attributes: {
                  schemeID: 'urn:cef.eu:names:identifier:EAS:0096',
                },
                _text: 'DK22233223',
              },
              'sdg:Name': {
                _attributes: {
                  lang: 'en',
                },
                _text: 'Denmark University Portal',
              },
              'sdg:Address': {
                'sdg:FullAddress': {
                  _text: 'Prince Street 15',
                },
                'sdg:LocatorDesignator': {
                  _text: '15',
                },
                'sdg:PostCode': {
                  _text: '1050',
                },
                'sdg:PostCityName': {
                  _text: 'Copenhagen',
                },
                'sdg:AdminUnitLevel1': {
                  _text: 'Denmark',
                },
                'sdg:AdminUnitLevel2': {
                  _text: 'DK011',
                },
              },
              'sdg:Classification': {
                _text: 'IntermediaryPlatform',
              },
            },
          },
        },
      },
      {
        _attributes: {
          name: 'EvidenceRequester',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:AnyValueType',
          },
          'sdg:Agent': {
            'sdg:Identifier': {
              _attributes: {
                schemeID: 'urn:cef.eu:names:identifier:EAS:9930',
              },
              _text: 'DE73524311',
            },
            'sdg:Name': {
              _text: 'Civil Registration Office Berlin I',
            },
          },
        },
      },
      {
        _attributes: {
          name: 'ResponseAvailableDateTime',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:DateTimeValueType',
          },
          'rim:Value': {
            _text: '2022-05-30T15:00:00.000Z',
          },
        },
      },
    ],
    'rim:RegistryObjectList': [],
    'rim:VatResponse': {},
  },
};
