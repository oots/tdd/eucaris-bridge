import { HttpException, Injectable } from '@nestjs/common';
import {
  listPendingMessages,
  retrieveMessage,
  submitMessage,
} from '../../../lib/domibus/ws';
import {
  SubmitMessageHeaderParams,
  SubmitMessageBodyParams,
  SubmitMessageResponse,
  ListPendingMessagesResponse,
  requestPayload,
} from '../../../lib/domibus';
import { isError } from './../../utils/result';
import { Config } from 'src/config/configuration';
import { ConfigService } from '@nestjs/config';
import { payloadJSON, requestData } from './constants';
import { js2xml } from 'xml-js';
import {
  LegalPersonSlot,
  NaturalPersonSlot,
} from 'lib/domibus/types/external/oots';
import { Cron, CronExpression } from '@nestjs/schedule';
import DatastoreService from '../datastore/datastore.service';
import { MessageLog } from '../datastore/datastore.interface';

@Injectable()
export class ClientService {
  private wsdlUrl: string;

  constructor(
    private configService: ConfigService<Config, true>,
    private datastore: DatastoreService,
  ) {
    this.wsdlUrl = `${configService.get<string>('CLIENT_ACCESS_POINT')}?wsdl`;
  }

  async getMessageById(id: string): Promise<MessageLog | undefined> {
    const result = this.datastore.getMessageLogs(id);
    let finalResult = undefined;

    if (result) {
      finalResult = result.data;
    }

    return finalResult;
  }

  async getPendingMessages(): Promise<ListPendingMessagesResponse> {
    const result = await listPendingMessages(this.wsdlUrl);
    const isErr = isError(result);
    if (isErr) {
      throw new HttpException(result.error.toString(), 500);
    }
    return result.data;
  }

  async submitMessage(payload: requestPayload): Promise<SubmitMessageResponse> {
    const headerParams: SubmitMessageHeaderParams = {
      fromParty: {
        id: requestData.edelivery_request.c2_party_id,
        type: requestData.edelivery_request.c2_party_id_type,
        role: requestData.edelivery_request.c2_role,
      },
      toParty: {
        id: requestData.edelivery_request.c3_party_id,
        type: requestData.edelivery_request.c3_party_id_type,
        role: requestData.edelivery_request.c3_role,
      },
      collaborationInfo: {
        action: requestData.action,
        service: requestData.service,
        type: requestData.type,
      },
      messageInfo: {
        originalSender: requestData.edelivery_request.c1_party_id,
        finalRecipient: requestData.edelivery_request.c4_party_id,
      },
    };

    let person: NaturalPersonSlot | LegalPersonSlot;

    if (payload.data.typeOfPerson === 0) {
      const NaturalPerson: NaturalPersonSlot = {
        _attributes: {
          name: 'NaturalPerson',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:AnyValueType',
          },
          'sdg:Person': {
            'sdg:LevelOfAssurance': {
              _text: 'High',
            },
            'sdg:FamilyName': {
              _text: payload.data.surName,
            },
            'sdg:GivenName': {
              _text: payload.data.name,
            },
            'sdg:DateOfBirth': {
              _text: payload.data.dateOfBirth.replace(/-/g, ''),
            },
          },
        },
      };

      person = NaturalPerson;
    } else {
      const LegalPerson: LegalPersonSlot = {
        _attributes: {
          name: 'LegalPerson',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:AnyValueType',
          },
          'sdg:LegalPerson': {
            'sdg:LevelOfAssurance': {
              _text: 'High',
            },
            'sdg:LegalName': {
              _text: payload.data.name,
            },
          },
        },
      };

      person = LegalPerson;
    }

    const payloadJSONReq = payloadJSON;
    payloadJSONReq['query:QueryRequest']['query:Query']['rim:Slot'] = [person];

    const payloadXML = js2xml(payloadJSONReq, {
      compact: true,
      spaces: 4,
    });

    const bodyParams: SubmitMessageBodyParams = {
      payload: {
        parts: [
          {
            href: 'message',
            contentType: 'text/xml',
            value: Buffer.from(payloadXML).toString('base64'),
          },
        ],
      },
    };

    const result = await submitMessage(this.wsdlUrl, headerParams, bodyParams);
    const isErr = isError(result);

    if (isErr) {
      throw new HttpException(result.error.toString(), 500);
    }

    this.datastore.addMessageLog(result.data.messageId);

    return result.data;
  }

  @Cron(CronExpression.EVERY_5_SECONDS)
  async processMessages() {
    // Load pending messages
    const pendingMessagesResult = await listPendingMessages(this.wsdlUrl);

    // Check for errors
    if (isError(pendingMessagesResult)) {
      console.error(pendingMessagesResult.error);
      return;
    }

    const { messageIds } = pendingMessagesResult.data;

    console.log('Starting to process messages from client...');

    await Promise.all(
      messageIds.map(async (messageId) => {
        // Handle request
        await this.handleRequest(messageId);
      }),
    );

    console.log('Finished processing messages from client...');
  }

  async handleRequest(messageId: string) {
    console.log(
      `[${Date.now()}]: Starting to process message: ${messageId}... from client`,
    );

    // Retrieve message
    const messageResult = await retrieveMessage(this.wsdlUrl, messageId);

    // Check for errors
    if (isError(messageResult)) {
      console.error(messageResult.error);
      return;
    }

    const { header, body } = messageResult.data;

    const data = {
      header: JSON.stringify(header),
      body: JSON.stringify(body),
    };

    this.datastore.updateMessageLog(
      header.Messaging.UserMessage.MessageInfo?.RefToMessageId,
      data,
    );

    if (header.Messaging.UserMessage.MessageInfo?.RefToMessageId) {
      await this.getMessageById(
        header.Messaging.UserMessage.MessageInfo?.RefToMessageId,
      );
    }
  }
}

export default ClientService;
