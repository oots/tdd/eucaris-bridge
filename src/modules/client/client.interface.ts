export type EDeliveryRequest = {
  c1_party_id: string;
  c2_party_id: string;
  c2_party_id_type: string;
  c2_role: string;
  c3_party_id: string;
  c3_party_id_type: string;
  c3_role: string;
  c4_party_id: string;
};

export type RegrepRequest = {
  messageId: string;
  request_el: string;
  requester_id: string;
  requester_id_type: string;
  provider_id: string;
  provider_id_type: string;
};
