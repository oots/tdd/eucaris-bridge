import {
  OOTSQueryRequest,
  OOTSQueryResponse,
} from 'lib/domibus/types/external/oots';

export const requestData = {
  edelivery_request: {
    c1_party_id: 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:C1',
    c4_party_id: 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:C4',
    c2_party_id: 'oots-rel-ap1',
    c2_party_id_type:
      'urn:oasis:names:tc:ebcore:partyid-type:unregistered:oots-simulator',
    c2_role: 'http://sdg.europa.eu/edelivery/gateway',
    c3_party_id: 'oots-rel-ap2',
    c3_party_id_type:
      'urn:oasis:names:tc:ebcore:partyid-type:unregistered:oots-simulator',
    c3_role: 'http://sdg.europa.eu/edelivery/gateway',
  },
  action: 'ExecuteQueryRequest',
  service: 'QueryManager',
  type: 'urn:oasis:names:tc:ebcore:ebrs:ebms:binding:1.0',
};

export const payloadJSON: OOTSQueryRequest = {
  _declaration: {
    _attributes: {
      version: '1.0',
      encoding: 'utf-8',
    },
  },
  'query:QueryRequest': {
    _attributes: {
      id: '1.0',
      lang: 'en',
    },
    'rim:Slot': [
      {
        _attributes: {
          name: 'SpecificationIdentifier',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:StringValueType',
          },
          'rim:Value': {
            _text: 'oots-edm:v1.0',
          },
        },
      },
      {
        _attributes: {
          name: 'IssueDateTime',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:DateTimeValueType',
          },
          'rim:Value': {
            _text: '2021-02-14T19:20:30+01:00',
          },
        },
      },
      {
        _attributes: {
          name: 'PossibilityForPreview',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:BooleanValueType',
          },
          'rim:Value': {
            _text: 'true',
          },
        },
      },
      {
        _attributes: {
          name: 'ExplicitRequestGiven',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:BooleanValueType',
          },
          'rim:Value': {
            _text: 'true',
          },
        },
      },
      {
        _attributes: {
          name: 'EvidenceRequester',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:CollectionValueType',
          },
          'rim:Element': {
            _attributes: {
              'xsi:type': 'rim:AnyValueType',
            },
            'sdg:Agent': {
              'sdg:Identifier': {
                _attributes: {
                  schemeID: 'urn:cef.eu:names:identifier:EAS:0096',
                },
                _text: 'DK22233223',
              },
              'sdg:Name': {
                _attributes: {
                  lang: 'en',
                },
                _text: 'Denmark University Portal',
              },
              'sdg:Address': {
                'sdg:FullAddress': {
                  _text: 'Prince Street 15',
                },
                'sdg:LocatorDesignator': {
                  _text: '15',
                },
                'sdg:PostCode': {
                  _text: '1050',
                },
                'sdg:PostCityName': {
                  _text: 'Copenhagen',
                },
                'sdg:AdminUnitLevel1': {
                  _text: 'Denmark',
                },
                'sdg:AdminUnitLevel2': {
                  _text: 'DK011',
                },
              },
              'sdg:Classification': {
                _text: 'IntermediaryPlatform',
              },
            },
          },
        },
      },
      {
        _attributes: {
          name: 'EvidenceProvider',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:AnyValueType',
          },
          'sdg:Agent': {
            'sdg:Identifier': {
              _attributes: {
                schemeID: 'urn:cef.eu:names:identifier:EAS:9930',
              },
              _text: 'DE73524311',
            },
            'sdg:Name': {
              _text: 'Civil Registration Office Berlin I',
            },
          },
        },
      },
    ],
    'query:ResponseOption': {
      _attributes: {
        returnType: 'LeafClassWithRepositoryItem',
      },
    },
    'query:Query': {
      _attributes: {
        queryDefinition: 'DocumentQuery',
      },
      'rim:Slot': [],
    },
  },
};
