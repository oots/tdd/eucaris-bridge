import { Body, Controller, Get, HttpCode, Param, Post } from '@nestjs/common';
import { ClientService } from './client.service';
import {
  ListPendingMessagesResponse,
  requestPayload,
  SubmitMessageResponse,
} from '../../../lib/domibus';
import { MessageLog } from '../datastore/datastore.interface';

@Controller()
export class ClientController {
  constructor(private clientService: ClientService) {}

  @Get('/getMessageById/:id')
  getMessageById(@Param('id') id: string): Promise<MessageLog | undefined> {
    return this.clientService.getMessageById(id);
  }

  @Get('/getPendingMessages')
  getPendingMessages(): Promise<ListPendingMessagesResponse> {
    return this.clientService.getPendingMessages();
  }

  @Post('/submitMessage')
  @HttpCode(200)
  submitMessage(
    @Body() payload: requestPayload,
  ): Promise<SubmitMessageResponse> {
    return this.clientService.submitMessage(payload);
  }
}
