import { Module } from '@nestjs/common';
import ConfigModule from '../../config/configuration';
import DatastoreModule from '../datastore/datastore.module';
import { ClientController } from './client.controller';
import { ClientService } from './client.service';

@Module({
  imports: [ConfigModule, DatastoreModule],
  controllers: [ClientController],
  providers: [ClientService],
  exports: [ClientService],
})
export class ClientModule {}

export default ClientModule;
