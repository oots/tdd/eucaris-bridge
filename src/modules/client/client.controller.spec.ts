import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { ClientController } from './client.controller';
import { ClientService } from './client.service';
import FastifyFormBody from '@fastify/formbody';
import { HttpServer, INestApplication, ValidationPipe } from '@nestjs/common';
import type { FastifyInstance } from 'fastify';
import qs from 'qs';
import { submitMessage, SubmitMessageResponse } from '../../../lib/domibus';
import ConfigModule, { Config } from '../../config/configuration';
import { ConfigService } from '@nestjs/config';
import {
  SUBMIT_MESSAGE_EXAMPLE_1_EXPECTED_PAYLOAD,
  SUBMIT_MESSAGE_EXAMPLE_2,
} from '../../../lib/domibus/tests/constants';
import { sleep } from '../../../lib/domibus/tests/utils';

describe('ClientController', () => {
  let app: INestApplication;
  let server: HttpServer;
  let appController: ClientController;
  let sampleMsg: SubmitMessageResponse;
  let configService: ConfigService<Config, true>;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule],
      controllers: [ClientController],
      providers: [ClientService],
    }).compile();

    appController = moduleFixture.get<ClientController>(ClientController);
    configService =
      moduleFixture.get<ConfigService<Config, true>>(ConfigService);

    const fastifyAdapter = new FastifyAdapter();
    await fastifyAdapter.register(FastifyFormBody, {
      parser: (str: string) =>
        qs.parse(str, {
          // Parse up to 50 children deep
          depth: 50,
          // Parse up to 1000 parameters
          parameterLimit: 1000,
        }),
    });
    fastifyAdapter.enableCors({ methods: '*' });

    app = moduleFixture.createNestApplication<NestFastifyApplication>(
      fastifyAdapter,
      { bodyParser: false },
    );

    app.useGlobalPipes(new ValidationPipe({ transform: true }));
    await app.init();
    await (app.getHttpAdapter().getInstance() as FastifyInstance).ready();
    server = app.getHttpServer() as HttpServer;

    appController = app.get<ClientController>(ClientController);

    // sampleMsg = await appController.submitMessage(
    //   edelivery_request,
    //   action,
    //   service,
    //   type,
    //   payload,
    // );
  });

  describe('/GET /getPendingMessages', () => {
    it('should return message data by Id', async () => {
      expect.assertions(2);
      if (!sampleMsg.messageId) return;
      const response = await request(server).get('/getPendingMessages');
      expect(response.status).toBe(200);
      expect(response.body.messageIds).toBeDefined();
    });
  });

  describe('/GET /getMessageById/:id', () => {
    it('should return message data by Id', async () => {
      expect.assertions(2);

      // Send message to C3
      await submitMessage(
        `${configService.get<string>('SERVER_ACCESS_POINT')}?wsdl`,
        SUBMIT_MESSAGE_EXAMPLE_2.headerParams,
        SUBMIT_MESSAGE_EXAMPLE_2.bodyParams,
      );

      // Wait for message to be processed
      sleep(1000);

      // List pending messages
      let response = await request(server).get('/getPendingMessages');

      response = await request(server).get(
        `/getMessageById/${response.body.messageIds[0]}`,
      );
      expect(response.status).toBe(200);
      expect(response.body.body.payload).toStrictEqual(
        SUBMIT_MESSAGE_EXAMPLE_1_EXPECTED_PAYLOAD,
      );
    });
    it('should fail because message does not exist', async () => {
      expect.assertions(2);
      const response = await request(server).get(`/getMessageById/123`);
      expect(response.status).toBe(500);
      expect(response.body.message).toMatch(/(Message not found)/i);
    });
  });

  describe('/POST /submitMessage', () => {
    it('should submit a message and return Id', async () => {
      expect.assertions(2);
      const body = {
        edelivery_request,
        action,
        service,
        type,
        payload,
      };
      const response = await request(server).post('/submitMessage').send(body);
      expect(response.status).toBe(200);
      expect(response.body.messageId).toBeDefined();
    });
  });
});

const edelivery_request = {
  c1_party_id: 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:C1',
  c4_party_id: 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:C4',
  c2_party_id: 'oots-rel-ap1',
  c2_party_id_type:
    'urn:oasis:names:tc:ebcore:partyid-type:unregistered:oots-simulator',
  c2_role: 'http://sdg.europa.eu/edelivery/gateway',
  c3_party_id: 'oots-rel-ap2',
  c3_party_id_type:
    'urn:oasis:names:tc:ebcore:partyid-type:unregistered:oots-simulator',
  c3_role: 'http://sdg.europa.eu/edelivery/gateway',
};
const action = 'ExecuteQueryRequest';
const service = 'QueryManager';
const type = 'urn:oasis:names:tc:ebcore:ebrs:ebms:binding:1.0';
const payload = {
  parts: [
    {
      href: 'message1',
      contentType: 'text/xml',
      value:
        'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPGhlbGxvPndvcmxkPC9oZWxsbz4=',
    },
    {
      href: 'message2',
      contentType: 'text/xml',
      value:
        'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPGhlbGxvPndvcmxkPC9oZWxsbz4=',
    },
  ],
};
