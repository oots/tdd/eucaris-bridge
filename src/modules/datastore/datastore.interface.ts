export interface StoredObject<T> {
  created: number;
  data: T;
}

export type MessageLog = {
  header?: string;
  body?: string;
};

export type MessageLogs = StoredObject<MessageLog>;

export interface MessageLogsStore {
  [messageId: string]: MessageLogs;
}
