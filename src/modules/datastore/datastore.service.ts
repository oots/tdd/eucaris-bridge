import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
// import cloneDeep from 'lodash.clonedeep';
import { MessageLogsStore, MessageLog } from './datastore.interface';

export const MAX_SESSION_LIFETIME = 24 * 60 * 60 * 1000; // 24 hours

/**
 * Simple, in-memory datastore
 */
@Injectable()
export class DatastoreService {
  private readonly logger = new Logger(DatastoreService.name);

  private messageLogsStore: MessageLogsStore;

  constructor() {
    this.messageLogsStore = {};
  }

  addMessageLog(messageId: string | undefined): void {
    if (messageId) {
      this.messageLogsStore[messageId] = {
        created: Date.now(),
        data: { header: '', body: '' },
      };
    }
  }

  updateMessageLog(messageId: string | undefined, data: MessageLog): void {
    if (messageId && data && this.messageLogsStore[messageId]) {
      this.messageLogsStore[messageId].data.header = data.header;
      this.messageLogsStore[messageId].data.body = data.body;
    }
  }

  getMessageLogs(messageId: string) {
    if (this.messageLogsStore[messageId]) {
      return this.messageLogsStore[messageId];
    }

    return undefined;
  }

  @Cron(CronExpression.EVERY_DAY_AT_MIDNIGHT)
  clearMessageLogs(): void {
    this.messageLogsStore = {};
    this.logger.debug('Message logs have been cleared');
  }
}

export default DatastoreService;
