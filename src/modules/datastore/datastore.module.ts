import { Module } from '@nestjs/common';
import { DatastoreService } from './datastore.service';

@Module({
  imports: [],
  controllers: [],
  providers: [DatastoreService],
  exports: [DatastoreService],
})
export class DatastoreModule {}

export default DatastoreModule;
