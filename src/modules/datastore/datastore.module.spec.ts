import { randomUUID } from 'node:crypto';
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe, Logger } from '@nestjs/common';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { ScheduleModule } from '@nestjs/schedule';
import type { FastifyInstance } from 'fastify';
import { DatastoreModule } from './datastore.module';
import { DatastoreService } from './datastore.service';

describe('Datastore Module', () => {
  let app: INestApplication;
  let datastoreService: DatastoreService;

  const LOG = {
    messageId: '123',
    refToMessageId: '1234',
  };

  const mockedLogger = {
    debug: jest.fn(),
    log: jest.fn(),
    warn: jest.fn(),
    error: jest.fn(),
  };

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [ScheduleModule.forRoot(), DatastoreModule],
    }).compile();

    app = moduleFixture.createNestApplication<NestFastifyApplication>(
      new FastifyAdapter(),
    );

    Logger.overrideLogger(mockedLogger);

    app.useGlobalPipes(new ValidationPipe({ transform: true }));
    await app.init();
    await (app.getHttpAdapter().getInstance() as FastifyInstance).ready();

    datastoreService = moduleFixture.get<DatastoreService>(DatastoreService);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  afterAll(async () => {
    // Avoid jest open handle error
    await new Promise((r) => {
      setTimeout(r, 500);
    });
    await app.close();
  });

  describe('Message Logs Store', () => {
    it('should store and return Message logs', () => {
      expect.assertions(4);

      const messageUuid = randomUUID();

      const log = LOG;

      // 1. At first, the datastore is empty
      expect(datastoreService.getMessageLogs(messageUuid)).toBeUndefined();

      // 2. When we push a new log, we can retrieve it
      datastoreService.addMessageLog(messageUuid, log);
      expect(datastoreService.getMessageLogs(messageUuid)).toStrictEqual({
        created: expect.any(Number) as number,
        data: log,
      });

      // 3. What happens if I edit the local Message log without adding it to the datastore?
      // The returned object should be (deep, not shallow) equal to the *original* object

      // FIXME: Fix this when we define the structure of the logs
      // log.request.url = '/new-url';
      expect(datastoreService.getMessageLogs(messageUuid)).toStrictEqual({
        created: expect.any(Number) as number,
        data: log,
      });

      // 4. Clear logs
      datastoreService.clearMessageLogs();
      expect(datastoreService.getMessageLogs(messageUuid)).toBeUndefined();
    });
  });
});
