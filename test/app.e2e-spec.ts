import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  // it('/client/submitMessage (POST)', async () => {
  //   const response = await request(app.getHttpServer())
  //     .post('/client/submitMessage')
  //     .send({
  //       name: 'Vries',
  //       surName: 'Donna',
  //       dateOfBirth: '02-02-1990',
  //     });

  //   expect(response.status).toBe(200);
  //   expect(response.body).toEqual({
  //     messageId: expect.any(String),
  //   });
  // });
});
